# Sviluppo di Giochi Digitali
###### _Autori: Luca Dimarco - Federico Cuccì_
###### _Anno di realizzazione: 2019_

## Contenuto
In questo repository vi sono il progetto di un videogioco in formato .zip e il gioco stesso, con all'interno l'eseguibile.

Il videogioco è stato realizzato con la versione `2018.3.14f1` di Unity.